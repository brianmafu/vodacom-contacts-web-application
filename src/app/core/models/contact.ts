export interface Contact {
  id?: number;
  title: string;
  name: string;
  email: string;
  phone?: string;
  description?: string;

}
