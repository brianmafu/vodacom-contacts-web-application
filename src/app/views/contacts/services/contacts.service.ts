import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Contact } from '@app/core/models';
import { environment } from '@app/env';


@Injectable()
export class ContactsService {

  constructor(private http: HttpClient) { }
  private api_url = "http://localhost:5000/"

  index(): Observable<Contact[]> {
    return this.http
      .get<Contact[]>(`${this.api_url}api/contact/`);
  }

  show(conactId: number): Observable<Contact> {
    return this.http
      .get<Contact>(`${this.api_url}api/contact/profile/${conactId}`);
  }

  create(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(`${this.api_url}api/contact/create`, contact);
  }

  update(contact: Partial<Contact>): Observable<Contact> {
    return this.http.patch<Contact>(`${this.api_url}api/contact/update/${contact.id}/`, contact);
  }


  destroy(id: number): Observable<Contact> {
    return this.http.delete<Contact>(`${this.api_url}api/contact/delete/${id}`);
  }

}
