import { Observable, of } from 'rxjs';
import { Contact } from '@app/core/models';


export class ContactsServiceMock {

  contacts = [{
    id: 1,
    name: 'johwen',
    email: 'john@gmail.com',
    title: 'mr'
  }, {
    id: 2,
    name: 'adamee',
    email: 'adam@gmail.com',
    title: 'mr'

  }];

  index(): Observable<Contact[]> {
    return of(this.contacts);
  }

  show(conactId: number): Observable<Contact> {
    return of({
      id: 1,
      name: 'johnee',
      email: 'john@gmail.com',
      title: 'mr'
    });
  }

  create(contact: Contact) {
    return of({
      id: 4,
      name: 'jesus',
      email: 'jesus@gmail.com',
      title: 'mr'
    });
  }

  destroy(id: number): Observable<number> {
    return of(1);
  }

  update(contact: Contact): Observable<Contact> {
    return of(contact);
  }

}
